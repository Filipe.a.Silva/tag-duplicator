import subprocess
import sys
from string import Template

curlTemplate = Template("curl -H \"PRIVATE-TOKEN: $${ACCESS_TOKEN}\" \"$${CI_API_V4_URL}/projects/$ID/repository/files/$scriptname/raw?ref=master\" -o $scriptname")
DownloadTemplate = Template("Downloaded $scriptname")
#$URL repo.tmp path.temp branch.tmp
repositoryID = 26172441
filesToTransfer = sys.argv[1:]
for curFile in filesToTransfer:
    print(curlTemplate.substitute(ID=repositoryID,scriptname=curFile))
    subprocess.call(curlTemplate.substitute(ID=repositoryID,scriptname=curFile),shell=True)
    print(DownloadTemplate.substitute(scriptname=curFile))
